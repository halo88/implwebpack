export default class Project{
  constructor(name, duration, createTime, desc, updateTime){
    this.name = name;
    this.duration = duration;
    this.createTime = createTime;
    this.desc = desc;
    this.updateTime = updateTime;
  }
}
