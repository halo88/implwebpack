import React, {Component} from 'react';
import { render } from 'react-dom';
import App from './components/App';
import { createStore } from 'redux';
import {reducer} from './redux/initRedux';
let list = {
  "sort":"Alphabet",
  "name":"",
  "category":""
};
const initialState = { list : list };
const store = createStore(reducer, initialState);
render(<App store={store}/>, document.getElementById('app'));
