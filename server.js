var express = require('express')
var app = express();

class Project{
  constructor(name, duration, createTime, desc, updateTime){
    this.name = name;
    this.duration = duration;
    this.createTime = createTime;
    this.desc = desc;
    this.updateTime = updateTime;
  }
}
let fakedata = {
  1: new Project('project 1', 30, new Date(), 'desc for project 1', new Date()),
  2: new Project('project 2', 30, new Date(), 'desc for project 2', new Date()),
  3: new Project('project 3', 30, new Date(), 'desc for project 3', new Date()),
  4: new Project('project 4', 30, new Date(), 'desc for project 4', new Date()),
  5: new Project('project 5', 30, new Date(), 'desc for project 5', new Date()),
  6: new Project('project 6', 30, new Date(), 'desc for project 6', new Date()),
}
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.listen(3030,()=>{
  console.log('server is running on port '+ 3030);
});
app.get("/api/getListAlphabet",  (req,res)=>{
  res.json({
    ...fakedata,
    sort:"Alphabet"
  });
})
app.get("/api/getListRecent",  (req,res)=>{
  res.json({
    ...fakedata,
    sort:"Recent"
  });
})
app.get("/api/getListDuration",  (req,res)=>{
  res.json({
    ...fakedata,
    sort:"Duration"
  });
})
