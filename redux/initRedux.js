import axios from 'axios';
export const types={
  A:"Alphabet",
  B:"Recent",
  C:"Duration"
}
export const reducer = (state, action) => {
  const list = state.list;
  const type = action.type;

  switch(type){
    case types.A:{
    //get list sorted Alphabet
    console.log('get list sorted Alphabet');
    axios.get('http://localhost:3030/api/getListAlphabet').then((data)=>{
      console.log(data)
      this.list = {
        ...data.data,
        sort:"Alphabet"
      }
    })
      return {
        ...state,
        list:this.list
      };
    }
    case types.B:{
      console.log('get list sorted Recent');
      //get list sorted Recent

      this.list = getRecent();
      console.log('getrecent')
      console.log(this.list.PromiseValue);

      // axios.get('http://localhost:3030/api/getListRecent').then((data)=>{
      // console.log(data);
      //   this.list = {
      //     ...data.data,
      //     sort:"Recent"
      //   }
      // })
      // return {
      //   ...state,
      //   list:this.list
      // };
    }
    case types.C:{
      //get list sorted Duration
      axios.get('http://localhost:3030/api/getListDuration').then((data)=>{
        this.list = {
          ...data.data,
          sort:"Duration"
        }
      })

      return {
        ...state,
        list:this.list
      };
    }
    default: console.log("default");
  }
  return state;
}
async function getRecent(){
  const res = await axios.get('http://localhost:3030/api/getListRecent')
  return res.data;
}
