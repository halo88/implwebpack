import Axios from 'axios';
import React, {Component} from 'react';

export default class GetJ extends Component {
    constructor() {
        super();
        this.state = {'mess': 'default'}
    };

    componentWillMount() {
        Axios.get('/api/hello').then(result => {
            return result.data;
        }).then(data => {
            this.setState({'mess': data.message});
            console.log(data);
        }).catch(e => {
            console.log(e);
        })
    }

    render() {
        return (
            <div>{this.state.mess}</div>
        )
    }

}
