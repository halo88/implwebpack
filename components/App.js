import React, {Component} from 'react';
import GetJ from "./GetJ";
import TopBar from "./TopBar.js";
import HeaderBanner from "./HeaderBanner.js";
import GridItem from "./GridItem";
import Grid from "./Grid";
import TabHome from "./TabHome";
import '../styles/codelabmain.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class App extends Component {
  componentWillMount(){
    const store = this.props.store;
    const list = store.getState().list;
    this.setState( {'list' : list} );
    this.unsubscribe = store.subscribe(()=>{
      this.setState({ list:list });

    })
  }
  componentWillUnmount(){
    this.unsubscribe();
  }


    render() {
      console.log('rendering APP');
      const store = this.props.store;
        return (
            <div className="App">
                <TopBar/>
                <div className="container-fluid">
                    <HeaderBanner/>
                </div>
                <div className="container">
                    <MuiThemeProvider>
                      <TabHome store={store}/>
                    </MuiThemeProvider>
                </div>
            </div>
        );
    }
}

export default App;
