import React from 'react';
import GridItem from './GridItem';
import '../styles/codelabmain.css';

export default class Grid extends React.Component {
    render() {
        return (
            <div className="row">
                <div id="cards" className="site-width">
                  <GridItem/>
                  <GridItem/>
                  <GridItem/>
                  <GridItem/>
                  <GridItem/>
                  <GridItem/>
                </div>
            </div>
        )
    }
}
