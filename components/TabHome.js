import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import {types} from '../redux/initRedux';
import GridItem from './GridItem';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  tab:{
    width:'30%'
  }
};

export default class TabHome extends React.Component {

  constructor(props) {
    super(props);
    // this.state = {
    //   value: 'A',
    // };
  }

  handleChange = (value) => {
    const store = this.props.store;
    console.log('handleChange:' + value);
    switch (value){
      case 'A':{
        store.dispatch({type : types.A});
        break;
      }
      case 'B':{
        store.dispatch({type : types.B});
        console.log(store.getState());
        break;
      }
      case 'C':{
        store.dispatch({type : types.C});
        break;
      }
    }

    // this.setState({
    //   value: value,
    // });
  };

  render() {
    console.log('rendering TabHome');
    const store = this.props.store;
    const list = store.getState().list;
    console.log(store.getState());
    const sort = list.sort;
    const name = list.name;
    const category = list.category;
    console.log(list);
    return (
      <div className="row site-width">

            <Tabs onChange={this.handleChange}>
              <Tab label="A-Z" style={styles.tab} value="A">

              </Tab>
              <Tab label="Recent" style={styles.tab} value="B">

              </Tab>
              <Tab label="Duration" style={styles.tab} value="C">

              </Tab>

            </Tabs>
            <div id="cards" className="site-width">
              {sort}
            </div>
      </div>
    );
  }
}
