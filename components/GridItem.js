import React, {Component} from 'react';
import "../styles/codelabmain.css";

export default class GridItem extends Component {
    constructor(props){
        super(props);
        // this.props={
            // data-category
            // data-title
            // data-updated
            // description
        // }
    }
    render() {
        return (
            <a href="https://codelabs.developers.google.com/codelabs/appauth-android-codelab/index.html?index=..%2F..%2Findex"
               className="codelab-card category-android inline" data-category="Android"
               data-title="Achieving Single Sign-on with AppAuth"
               data-duration="39" data-updated="2016-05-20T04:42:50Z"
               data-tags="android,io2016,kiosk" data-pin="" data-wait-for-ripple="">
                <div className="card-header android-bg">
                    <div id="icon" className="category-icon android-icon"></div>
                    <div className="card-duration">
                        <img src="https://codelabs.developers.google.com/images/schedule.svg"/>
                        <span>{this.props.item.duration} min</span>
                    </div>
                </div>
                <div className="description">{this.props.item.desc}</div>
                <div className="card-footer">
                    <paper-button role="button" tabindex="0" animated="" aria-disabled="false"
                                  elevation="0">
                        Start
                    </paper-button>
                    <div>
                        <div className="card-updated">{this.props.item.updateTime}</div>
                    </div>
                </div>
            </a>
        )
    }
}
