import React, {Component} from 'react';
import "../styles/codelabmain.css";
// import "bootstrap/dist/css/bootstrap.css";
import "../styles/topbar.css";
//import '../model/Project';
//import {Project} from "../model/Project";
//import DocumentStore from 'ravendb';

//let raven = require('../../node_modules/node-ravendb');

class TopBar extends Component {
    // constructor(props) {
    //     super();
    //
    // }

    async a() {
      //  var store = new raven.Store({host: '127.0.0.1', port: 5000, database: 'test'}).initialize();
      //  var session = store.openSession();
      //  let pro = new Project('category', 'title', 'updated', 'description');
      //  console.log('outside callback');
        // session.load('abc', function (error, document) {
        //     console.log('enter callback');
        //     if (!error) {
        //         console.log('=====************==========');
        //         console.log(document); // { name: 'Max' ... }
        //     }
        //     document.asd = pro;
        //     session.save();
        //     console.log('under callback');
        // });
  //      session.load('project/1',(err,data)=>{
  //          console.log(data);
  //      });
  //      let pro2 = {"name":"abc", "demo":"demo"};
  //      await session.store(pro,'abc');
  //      console.log(pro);
  //      session.save(function(err,data){
  //          console.log("saving");
  //          if (!err){
    //            console.log(data);
      //      }else{
    //            console.log(err);
  //          }
    //    });
    }
    render() {
//        ok();
        // this.a();
        return (
            <div id="mainToolbar" className="paper-header">
                <div className="site-width layout horizontal">
                    <a href="https://codelabs.developers.google.com/" title="Google Developers">
                        <img src="https://codelabs.developers.google.com/images/developers-logo.svg"
                             className="logo-icon"/>
                        <img src="https://codelabs.developers.google.com/images/lockup_developers_light_color.svg"
                             className="logo-devs"/>
                    </a>
                    <div className="search">
                        <button className="searchBtn"><span className="glyphicon glyphicon-search"></span></button>
                        <input className="searchInput" placeholder="Search"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default TopBar;
